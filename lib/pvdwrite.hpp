//
//  pvdwrite.hpp
//  rect_4
//
//  Created by Hiroshi Sugimoto on 2018/08/28.
//  Copyright © 2018年 Hiroshi Sugimoto. All rights reserved.
//  Try It.

#ifndef pvdwrite_hpp
#define pvdwrite_hpp

#include <iostream>
#include <fstream>
#include <map>
#include <boost/filesystem.hpp>

class PVDwrite {
    boost::filesystem::path _filename;
    boost::filesystem::path _basedir;
    std::map<double,boost::filesystem::path> _array;
public:
    void Append(boost::filesystem::path &P,double time);
    void Write();
    PVDwrite(boost::filesystem::path filename);
    ~PVDwrite();
};

#endif /* pvdwrite_hpp */
