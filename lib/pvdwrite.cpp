//
//  pvdwrite.cpp
//  rect_4
//
//  Created by Hiroshi Sugimoto on 2018/08/28.
//  Copyright © 2018年 Hiroshi Sugimoto. All rights reserved.
//

#include "pvdwrite.hpp"

void PVDwrite::Append(boost::filesystem::path &P,double time){
	_array[time]=P;
};
void PVDwrite::Write(){
	boost::filesystem::ofstream ofs(_filename);
	ofs << "<?xml version=\"1.0\"?>";
	ofs << "<VTKFile type=\"Collection\" version=\"0.1\" byte_order=\"LittleEndian\" compressor=\"vtkZLibDataCompressor\" >";
	ofs << "<Collection>" << std::endl;
	for(auto& pair:_array) ofs << "<DataSet timestep=\"" << pair.first << "\" group=\"\" part=\"0\" file=" << boost::filesystem::relative(pair.second,_basedir) << " />" << std::endl;
	ofs << "</Collection></VTKFile>" << std::endl;
};
PVDwrite::PVDwrite(boost::filesystem::path filename):_filename(filename){
    _basedir=_filename.parent_path();
};
PVDwrite::~PVDwrite(){
	_array.clear();
};
