
//
//  rect_5: VTKを使って，セルデータから格子データを自動計算する例
//

#include <boost/filesystem.hpp>
#include <iostream>
#include <vector>
#include <cmath>
#include <sstream>
#include <vtkVersion.h>
#include <vtkSmartPointer.h>
#include <vtkProperty.h>
#include <vtkStructuredGrid.h>				//構造格子を作成
#include <vtkImageData.h>                      //等分割直方格子の領域を使います
#include <vtkMultiBlockDataSet.h>            //１ケースに多数の領域が含まれる場合，これが必要
#include <vtkXMLMultiBlockDataWriter.h>    //多数の領域を一発で保存する
#include <vtkDoubleArray.h>                   //セルや格子点に取り付ける物理量を定義します. libvtkCommonCore 必要
#include <vtkPointData.h>                      //格子点のデータ
#include <vtkCellData.h>                       //セルのデータ
#include <vtkCell.h>                            //セル
#include <vtkCellDataToPointData.h>
#include "pvdwrite.hpp"

int main(int argc, const char * argv[]) {
	//時間ステップの作成
	const unsigned int ntMax=3;
	const double dt=0.2;
	double time=0;
	boost::filesystem::path data_dir=getenv("HOME");
	data_dir=data_dir/"Data"/"VTK";
	const boost::filesystem::path bodyFolder(data_dir/"struct.3");
	const boost::filesystem::path wallFolder(data_dir/"struct.3");
	boost::filesystem::path bodyStemName("data/body");//ファイル名はbodyFolder/bodyStemNameN桁の数字extName
	boost::filesystem::path wallStemName("data/wall");//ファイル名はwallFolder/wallStemNameN桁の数字extName
	const std::string extName=".vtm";        //複数領域では，拡張子は .vtm です
	const int fileN=4;                           //まあテストなので, 4桁あれば十分
	//PVDファイルを作成する
	PVDwrite BodyTime(bodyFolder/"body.pvd");
	PVDwrite WallTime(wallFolder/"wall.pvd");
	//領域の作成
	const unsigned int domMax=2;                //2領域作成しまーす
	//フォルダーの作成(既存のフォルダーは黙って削除)
	try {
		if (boost::filesystem::exists(bodyFolder)) boost::filesystem::remove_all(bodyFolder);
		if (boost::filesystem::exists(wallFolder)) boost::filesystem::remove_all(wallFolder);
		boost::filesystem::create_directories(bodyFolder);
		boost::filesystem::create_directories(wallFolder);
	} catch (std::exception& e) {
		std::cerr << bodyFolder << " " << wallFolder << " フォルダが作成できんとよ:" << e.what() << std::endl;
		return 1;
	}
	bodyStemName=bodyFolder/bodyStemName;
	wallStemName=wallFolder/wallStemName;
	//面倒なので
	using StructPtr = vtkSmartPointer<vtkStructuredGrid>;
	using PointPtr=vtkSmartPointer<vtkPoints>;
	//時間ステップだけ作業
	for(unsigned int nt=1;nt<=ntMax;nt++){
		time=nt*dt;
		std::cout << std::endl << "time=" << time << std::endl;
		//この時刻のデータを保存するMultiBlockDataSetを定義
		auto body=vtkSmartPointer<vtkMultiBlockDataSet>::New();
		auto wall=vtkSmartPointer<vtkMultiBlockDataSet>::New();
		//領域達を定義, セルデータを追加
		for(int idom=0;idom<domMax;idom++){
			int Dimentor[3];				//領域の格子点数．あとで設定します
			auto iPoint=PointPtr::New();	//格子点を定義します
			switch (idom) {//格子点を定義
				case 0:    //領域0のパラメータ
					Dimentor[0]=10;
					Dimentor[1]=10;
					Dimentor[2]=10;
					for(int k=0;k<=Dimentor[2];k++)
					for(int j=0;j<=Dimentor[1];j++)
					for(int i=0;i<=Dimentor[0];i++) {
						double x[3];
						x[0]=i*0.1;
						x[1]=j*0.1*(1.+x[0]*0.2);
						x[2]=k*0.1*(1.+x[0]*0.4);
						iPoint->InsertNextPoint(x[0],x[1],x[2]);
					}
					break;
					;;
				case 1:    //領域1のパラメータ
					Dimentor[0]=10;
					Dimentor[1]=20;
					Dimentor[2]=20;
					for(int k=0;k<=Dimentor[2];k++)
					for(int j=0;j<=Dimentor[1];j++)
					for(int i=0;i<=Dimentor[0];i++)
					{
						double x[3] {i*0.1+1,j*0.1-0.5,k*0.1-0.5};
						iPoint->InsertNextPoint(x[0],x[1],x[2]);
					}
					break;
					;;
			}
			auto iBody=StructPtr::New();
			iBody->SetDimensions(Dimentor[0]+1,Dimentor[1]+1,Dimentor[2]+1);
			iBody->SetPoints(iPoint);
			body->SetBlock(idom,iBody);
			//bodyに物理量を定義:ここでは，グリッドに物理量を準備してみましょう．セルの場合 PointsをCellに書き換えます
			auto pressure = vtkDoubleArray::New();        //圧力の配列を作成
			auto velocity = vtkDoubleArray::New();        //流速の配列を作成
			pressure->SetNumberOfComponents(0);                        //圧力はスケーラなので成分は無いっす
			velocity->SetNumberOfComponents(3);                        //数学者以外は流速は3成分すね
			pressure->SetName("p");                    //名前を付けておきませう
			velocity->SetName("v");
			velocity->SetComponentName(0,"v_1");
			velocity->SetComponentName(1,"v_2");
			velocity->SetComponentName(2,"v_3");
			pressure->SetNumberOfTuples(iBody->GetNumberOfCells());    //圧力の要素数は, セルの総数
			velocity->SetNumberOfTuples(iBody->GetNumberOfCells());    //流速の要素数は, セルの総数
			iBody->GetCellData()->AddArray(pressure);    //領域のセルデータに，さっき作った圧力を追加
			iBody->GetCellData()->AddArray(velocity);    //さっき作った流速を追加
			//まあ，テストのために値を定義しておきましょう
			double* weight = new double [iBody->GetMaxCellSize()];
			for(vtkIdType cellId=0; cellId < iBody->GetNumberOfCells(); cellId++) {
				//格子点の場合には image->GetPoint(pointId,Xcoord)で取得できますが, セルは面倒
				double Xcoord[3];
				{
					auto cell=iBody->GetCell(cellId);
					double Pcoord[3];    //セルは体積要素ですので，内部にいちいち曲線座標が存在します
					//曲線座標表示で, セルの中心を求めます. 複合セルの場合のsubIdも取得(普通subId=0)
					int subId= cell->GetParametricCenter(Pcoord);
					cell->EvaluateLocation(subId,Pcoord,Xcoord,weight); //　ようやく, セル中心のXcoordが取得できました♪
				}
//				std::cout << "Cell " << cellId << " Position= " << Xcoord[0] << " " << Xcoord[1] << " " << Xcoord[2] << std::endl;
				//値を計算（サンプルなので，テキトーですわ）
				double T=pow(Xcoord[0],2)+pow(Xcoord[1],2)+pow(Xcoord[2],2);
				double p=exp(-0.1*T);
				double v1=0.1;
				double v2=p*Xcoord[1];
				double v3=-0.1;
				//値を設定
				pressure->SetValue(cellId,p);
				velocity->SetComponent(cellId,0,v1);
				velocity->SetComponent(cellId,1,v2);
				velocity->SetComponent(cellId,2,v3);
			}
//			std::cout << "Point Data:" << std::endl;
//			ibody->GetPointData()->PrintSelf(std::cout,vtkIndent(3));
//			std::cout << "Cell Data:" << std::endl;
//			ibody->GetCellData()->PrintSelf(std::cout,vtkIndent(3));
//			//境界形状を定義
//			for(int iwall=0;iwall<6;iwall++) {
//				//wall形状を定義
//				auto image=ImageDataPtr::New();//上のaliasを用いてImageDataを作成
//				image->SetSpacing(imageSpacing);          //X-Y-Z方向の格子の間隔
//				int wallExtent[6];
//				double wallOrigin[6];
//				for(int iii=0;iii<6;iii++) {
//					wallExtent[iii]=bodyExtent[iii];
//					wallOrigin[iii]=bodyOrigin[iii];
//				}
//				std::string name;
//				switch (iwall) {
//					case 0: wallExtent[1]=wallExtent[0]; name="left";break;; //左面(X幅がない)
//					case 1: wallExtent[0]=wallExtent[1]; name="right";break;;//右面(X幅がない)
//					case 2: wallExtent[3]=wallExtent[2]; name="floor";break;;//下面(Y幅がない)
//					case 3: wallExtent[2]=wallExtent[3]; name="ceiling";break;;//上面(Y幅がない)
//					case 4: wallExtent[5]=wallExtent[4]; name="away";break;;//後面(Z幅がない)
//					case 5: wallExtent[4]=wallExtent[5]; name="home";break;;//前面(Z幅がない)
//				}
//				int iBlock=6*idom+iwall;
//				image->SetExtent(wallExtent);
//				image->SetOrigin(wallOrigin);
//				wall->SetBlock(iBlock,image);
//				//wallに物理量を定義:ここでは，セルに物理量を準備してみましょう．格子点の場合 Cell をPointに書き換えます
//				auto massFlux = vtkDoubleArray::New();      //質量流量の配列を作成
//				auto momFlux = vtkDoubleArray::New();        //運動量流量の配列を作成
//				massFlux->SetNumberOfComponents(0);         //質量流量はスケーラなので成分は無いっす
//				momFlux->SetNumberOfComponents(3);             //運動量流量は3成分すね
//				massFlux->SetName("M");                        //名前を付けておきませう
//				momFlux->SetName("P");
//				momFlux->SetComponentName(0,"P_1");
//				momFlux->SetComponentName(1,"P_2");
//				momFlux->SetComponentName(2,"P_3");
//				massFlux->SetNumberOfTuples(image->GetNumberOfCells());    //質量流量の要素数は, セルの総数
//				momFlux->SetNumberOfTuples(image->GetNumberOfCells());    //運動量流量の要素数は, セルの総数
//				image->GetCellData()->AddArray(massFlux);    //領域のセルデータに，さっき作った圧力を追加
//				image->GetCellData()->AddArray(momFlux);    //さっき作った流速を追加
//				//まあ，テストのために値を定義しておきましょう
//				double* weight = new double [image->GetMaxCellSize()];
//				for(vtkIdType cellId=0; cellId < image->GetNumberOfCells(); cellId++) {
//					//テキトーな値を計算するために, セルの中心座標を取得します.
//					//格子点の場合には image->GetPoint(pointId,Xcoord)で取得できますが, セルは面倒
//					double Xcoord[3];
//					{
//						auto cell=image->GetCell(cellId);
//						double Pcoord[3];    //セルは体積要素ですので，内部にいちいち曲線座標が存在します
//						//曲線座標表示で, セルの中心を求めます. 複合セルの場合のsubIdも取得(普通subId=0)
//						int subId= cell->GetParametricCenter(Pcoord);
//						cell->EvaluateLocation(subId,Pcoord,Xcoord,weight); //　ようやく, セル中心のXcoordが取得できました♪
//					}
//					//値を計算（サンプルなので，テキトーですわ）
//					double T=pow(Xcoord[0],2)+pow(Xcoord[1],2)+pow(Xcoord[2],2);
//					double p=sin(-0.1*T);
//					double v1=0.1*p*cos(Xcoord[2]);
//					double v2=p*Xcoord[1];
//					double v3=0.1*p*sin(Xcoord[2]);
//					//値を設定
//					massFlux->SetValue(cellId,p);
//					momFlux->SetComponent(cellId,0,v1);
//					momFlux->SetComponent(cellId,1,v2);
//					momFlux->SetComponent(cellId,2,v3);
//				}
//
//			}
		}
		std::cout << "---Add point data---" << std::endl;
		//格子データを追加
		for(int idom=0;idom<domMax;idom++){
			auto ibody=body->GetBlock(idom);//BlockDataへのポインタ
			StructPtr ptr=static_cast<vtkStructuredGrid*>(ibody);
			auto* C2P=vtkCellDataToPointData::New();
			C2P->PassCellDataOn();
			C2P->SetInputData(ptr);
			C2P->Update();
			StructPtr new_ptr=C2P->GetStructuredGridOutput();
			ptr->ShallowCopy(new_ptr);
		}
//		for(int idom=0;idom<domMax;idom++){
//			auto ibody=body->GetBlock(idom);//BlockDataへのポインタ
////			vtkSmartPointer<vtkImageData> img=static_cast<vtkImageData*>(ibody);
//			vtkSmartPointer<vtkStructuredData> img=static_cast<vtkImageData*>(ibody);
//			std::cout << "dom[" << idom << "] size="
//			<< img->GetExtent()[0] << ":" << img->GetExtent()[1] << " x "
//			<< img->GetExtent()[2] << ":" << img->GetExtent()[3] << " x "
//			<< img->GetExtent()[4] << ":" << img->GetExtent()[5] << std::endl;
//			std::cout << "Point Data:" << std::endl;
//			img->GetPointData()->PrintSelf(std::cout,vtkIndent(3));
//			std::cout << "Cell Data:" << std::endl;
//			img->GetCellData()->PrintSelf(std::cout,vtkIndent(3));
//		}
		//      保存を実行
		std::cout<<"Saving..."<<std::endl;
		std::stringstream    fileNumber;
		fileNumber <<  std::setfill('0') <<  std::setw(fileN) << nt << extName;
		{
			boost::filesystem::path fileName(bodyStemName);
			fileName+=fileNumber.str();
			auto writer=vtkSmartPointer<vtkXMLMultiBlockDataWriter>::New();
			//writer->SetWriteMetaFile(1);   効き目なし
			writer->SetFileName(fileName.c_str());            //掃除屋にファイル名を設定
			writer->SetInputData(body);                        //掃除屋にボディーを引き渡す
			writer->Write();                                       //掃除を実行
			BodyTime.Append(fileName, time);					   //PVDインデックスに追加
		}
//		{
//			boost::filesystem::path fileName(wallStemName);
//			fileName+=fileNumber.str();
//			auto writer=vtkSmartPointer<vtkXMLMultiBlockDataWriter>::New();
//			writer->SetFileName(fileName.c_str());           //掃除屋にファイル名を設定
//			writer->SetInputData(wall);                        //掃除屋にボディーを引き渡す
//			writer->Write();                                      //掃除を実行
//			WallTime.Append(fileName, time);                       //PVDインデックスに追加
//		}
		std::cout << " nt=" << nt << " done." << std::endl;
	}
	BodyTime.Write();
//	WallTime.Write();
	return 0;
}

