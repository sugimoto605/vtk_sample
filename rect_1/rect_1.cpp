//
//  rect_1: VTKを使って，複数の直方体領域のデータを作成する例
//

#include <boost/filesystem.hpp>
#include <iostream>
#include <vector>
#include <cmath>
#include <sstream>
#include <vtkVersion.h>
#include <vtkSmartPointer.h>
#include <vtkProperty.h>
#include <vtkImageData.h>						//等分割直方格子の領域を使います
#include <vtkMultiBlockDataSet.h>			//１ケースに多数の領域が含まれる場合，これが必要
#include <vtkXMLMultiBlockDataWriter.h>	//多数の領域を一発で保存する
#include <vtkDoubleArray.h>					//セルや格子点に取り付ける物理量を定義します. libvtkCommonCore 必要
#include <vtkPointData.h>						//格子点のデータ
#include <vtkCellData.h>						//セルのデータ
#include <vtkCell.h>							//セル

int main(int argc, const char * argv[]) {
    //時間ステップの作成
    const unsigned int ntMax=100;
    const boost::filesystem::path folderName("rect_1.dat");
    const std::string stemName="time";		//ファイル名は    folderName/stemName時間ステップ(N桁の数字)extName
    const std::string extName=".vtm";		//複数領域では，拡張子は .vtm です
    const int fileN=4;							//まあテストなので, 4桁あれば十分
    //領域の作成
    const unsigned int domMax=2;				//2領域作成しまーす
    //フォルダーの作成(既存のフォルダーは黙って削除)
    try {
        if (boost::filesystem::exists(folderName)) boost::filesystem::remove_all(folderName);
        boost::filesystem::create_directory(folderName);
    } catch (std::exception& e) {
        std::cerr << folderName << "フォルダが作成できんとよ:" << e.what() << std::endl;
        return 1;
    }
    //時間ステップだけ作業
    for(unsigned int nt=1;nt<=ntMax;nt++){
        //この時刻のデータを保存するMultiBlockDataSetを定義
        auto multi=vtkSmartPointer<vtkMultiBlockDataSet>::New();
        //領域達を定義
        for(int idom=0;idom<domMax;idom++){
            //ImageData形状を定義
            using ImageDataPtr = vtkSmartPointer<vtkImageData>; //行が長くなるので略称alias
            auto image=ImageDataPtr::New();//上のaliasを用いてImageDataを作成
            image->SetSpacing(0.05,0.05,0.05);		//X-Y-Z方向の格子の間隔
            int imageExtent[6]={0,0,0,0,0,0};		//領域の格子点数．あとで設定します
            double imageOrigin[3]={0.,0.,0.};		//領域の原点．あとで設定します
            switch (idom) {
                case 0:	//領域0のパラメータ
                    imageExtent[1]=20;		imageExtent[3]=10;		imageExtent[5]=10;
                    imageOrigin[0]=0;		imageOrigin[1]=0;		imageOrigin[2]=0;
                    break;
                    ;;
                case 1:	//領域1のパラメータ
                    imageExtent[1]=10;		imageExtent[3]=40;		imageExtent[5]=40;
                    imageOrigin[0]=1;		imageOrigin[1]=-0.5;	imageOrigin[2]=-0.5;
                    break;
                    ;;
            }
            image->SetExtent(imageExtent);
            image->SetOrigin(imageOrigin);
            //MultiBlockDataSetに追加
            multi->SetBlock(idom,image);
            //ImageDataに物理量を定義:ここでは，セルに物理量を準備してみましょう．格子点の場合 Cell をPointに書き換えます
            auto pressure = vtkDoubleArray::New();		//圧力の配列を作成
            auto velocity = vtkDoubleArray::New();		//流速の配列を作成
            pressure->SetNumberOfComponents(0);						//圧力はスケーラなので成分は無いっす
            velocity->SetNumberOfComponents(3);						//数学者以外は流速は3成分すね
            pressure->SetName("p");					//名前を付けておきませう
            velocity->SetName("v");
            velocity->SetComponentName(0,"v_1");
            velocity->SetComponentName(1,"v_2");
            velocity->SetComponentName(2,"v_3");
            pressure->SetNumberOfTuples(image->GetNumberOfCells());	//圧力の要素数は, セルの総数
            velocity->SetNumberOfTuples(image->GetNumberOfCells());	//流速の要素数は, セルの総数
            image->GetCellData()->AddArray(pressure);	//領域のセルデータに，さっき作った圧力を追加
            image->GetCellData()->AddArray(velocity);	//さっき作った流速を追加
            //まあ，テストのために値を定義しておきましょう
            double* weight = new double [image->GetMaxCellSize()];
            for(vtkIdType cellId=0; cellId < image->GetNumberOfCells(); cellId++) {
                //テキトーな値を計算するために, セルの中心座標を取得します.
                //格子点の場合には image->GetPoint(pointId,Xcoord)で取得できますが, セルは面倒
                double Xcoord[3];
                {
                    auto cell=image->GetCell(cellId);
                    double Pcoord[3];    //セルは体積要素ですので，内部にいちいち曲線座標が存在します
                    //曲線座標表示で, セルの中心を求めます. 複合セルの場合のsubIdも取得(普通subId=0)
                    int subId= cell->GetParametricCenter(Pcoord);
                    cell->EvaluateLocation(subId,Pcoord,Xcoord,weight); //　ようやく, セル中心のXcoordが取得できました♪
                }
                //値を計算（サンプルなので，テキトーですわ）
                double T=pow(Xcoord[0],2)+pow(Xcoord[1],2)+pow(Xcoord[2],2);
                double p=exp(-0.1*T);
                double v1=0.1;
                double v2=p*Xcoord[1];
                double v3=-0.1;
                //値を設定
                pressure->SetValue(cellId,p);
                velocity->SetComponent(cellId,0,v1);
                velocity->SetComponent(cellId,1,v2);
                velocity->SetComponent(cellId,2,v3);
            }
            std::cout << "dom[" << idom << "]";
        }
        //ファイル名を作成
        boost::filesystem::path fileName;
        {
            std::stringstream    sst;
            sst<< stemName <<  std::setfill('0') <<  std::setw(fileN) << nt << extName;
            fileName=sst.str();
            fileName = folderName / fileName;
        }
        //      保存を実行
        auto writer=vtkSmartPointer<vtkXMLMultiBlockDataWriter>::New();
        writer->SetFileName(fileName.c_str());		//掃除屋にファイル名を設定
		writer->SetInputData(multi);					//掃除屋にボディーを引き渡す
		writer->Write();									//掃除を実行
        std::cout << " nt=" << nt << " done." << std::endl;
    }
    return 0;
}
