//
//  main.cpp
//  filter_1
//
//  Created by Hiroshi Sugimoto on 2021/11/26.
//  Copyright © 2021 Hiroshi Sugimoto. All rights reserved.
//

#include <boost/filesystem.hpp>
#include <iostream>
#include <vector>
#include <cmath>
#include <sstream>
#include <Eigen/Dense>
#include <vtkVersion.h>
#include <vtkSmartPointer.h>
#include <vtkProperty.h>
#include <vtkPointLocator.h>
#include <vtkPointData.h>					//格子点のデータ
#include <vtkStructuredGrid.h>				//構造格子
#include <vtkMultiBlockDataSet.h>			//１ケースに多数の領域が含まれる場合，これが必要
#include <vtkXMLMultiBlockDataWriter.h>		//多数の領域を一発で保存する
#include <vtkDoubleArray.h>					//セルや格子点に取り付ける物理量を定義します. libvtkCommonCore 必要
#include <vtkInformation.h>					//vtkの名前
#include <vtkTransform.h>
#include <vtkTransformFilter.h>
//#include <vtkCellData.h>						//セルのデータ
//#include <vtkCell.h>							//セル

int main(int argc, const char * argv[]) {
	const unsigned int domMax=2;                				//2領域作成しまーす
	Eigen::Vector3d dX {0.1,0.1,0.1};
	Eigen::Vector3d Base[2];
	Base[0]<< 0,0,0;	Base[1]<< 1,0.5,0.5;
	Eigen::Vector3i Dim[2];
	Dim[0]<<11,11,11;	Dim[1]<<11,11,21;
	const unsigned int digit_VTK=10;							//VTKファイルの時刻桁数
	//作業フォルダーに移動
	boost::filesystem::path workdir=getenv("HOME");
	boost::filesystem::current_path(workdir/"tmp/VTK");
	//フォルダーの作成(既存のフォルダーは黙って削除)
	const boost::filesystem::path bodyFolder("filter_1.Body");
	try
	{
		if (boost::filesystem::exists(bodyFolder)) boost::filesystem::remove_all(bodyFolder);

		boost::filesystem::create_directory(bodyFolder);
	}
	catch (std::exception& e)
	{
		std::cerr << bodyFolder << " " << bodyFolder << " フォルダが作成できんとよ:" << e.what() << std::endl;
		return 1;
	}
	boost::filesystem::path BODYfile=bodyFolder/"Contents"/"body";//bodyFolder/Contents/bodyN桁の数字.vtm
	//時間ステップは1個だけ
	int nt=0;
	{
		auto body=vtkSmartPointer<vtkMultiBlockDataSet>::New();
		//領域達を定義
		for(int idom=0;idom<domMax;idom++)
		{
			//領域を作成
			auto Point=vtkSmartPointer<vtkPoints>::New();
			int idx[3];
			for(idx[2]=0;idx[2]<Dim[idom][2];idx[2]++)
			for(idx[1]=0;idx[1]<Dim[idom][1];idx[1]++)
			for(idx[0]=0;idx[0]<Dim[idom][0];idx[0]++)
			{
				Eigen::Vector3d xP;
				for(int m=0;m<3;m++)
					xP[m]=Base[idom][m]+idx[m]*dX[m];
				Point->InsertNextPoint(xP.data());
			}
			auto vtkData=vtkSmartPointer<vtkStructuredGrid>::New();
			vtkData->SetDimensions(Dim[idom].data());
			vtkData->SetPoints(Point);
			body->SetBlock(idom,vtkData);
			body->GetMetaData(idom)->Set(vtkCompositeDataSet::NAME(),"Region-"+std::to_string(idom));
			//スカラーデータ[pressure]を追加
			{
				auto pScalar = vtkDoubleArray::New();
				pScalar->SetNumberOfComponents(0);
				pScalar->SetName("pressure");
				pScalar->SetNumberOfTuples(vtkData->GetNumberOfPoints());	//スカラー量の要素数は, 格子点の総数
				vtkData->GetPointData()->AddArray(pScalar);
				//格子点の値を設定
				for(vtkIdType pId=0; pId < vtkData->GetNumberOfPoints(); pId++)
				{
					Eigen::Vector3d xP;	//格子点の座標を取得
					vtkData->GetPoint(pId,xP.data());
					double VAL=xP.norm();
					pScalar->SetValue(pId,VAL);
				}
			}
			//ベクトルデータ[velocity]を追加
			{
				auto pVector = vtkDoubleArray::New();
				pVector->SetNumberOfComponents(3);
				pVector->SetName("v");
				for(int m=0;m<3;m++) pVector->SetComponentName(m,(std::string(pVector->GetName())+"_"+std::to_string(m+1)).c_str());
				pVector->SetNumberOfTuples(vtkData->GetNumberOfPoints());	//スカラー量の要素数は, 格子点の総数
				vtkData->GetPointData()->AddArray(pVector);
				//格子点の値を設定
				for(vtkIdType pId=0; pId < vtkData->GetNumberOfPoints(); pId++)
				{
					Eigen::Vector3d xP;	//格子点の座標を取得
					vtkData->GetPoint(pId,xP.data());
					for(int m=0;m<3;m++)
						pVector->SetComponent(pId,m,xP[m]);
				}
			}
		}
		//変換をかけてみる
		for(int idom=0;idom<domMax;idom++)
		{
			auto vtkData=body->GetBlock(idom);
			//[1] 変換行列
			auto myMatrix=vtkSmartPointer<vtkMatrix4x4>::New();		//libvtkCommonMath
			myMatrix->Zero();
			myMatrix->SetElement(3,3,1.);	//同次座標系
			myMatrix->SetElement(0,0,1.);
			myMatrix->SetElement(1,1,1.);
			myMatrix->SetElement(2,2,1.);
			myMatrix->SetElement(0,1,0.1);
			//[2] 変換を定義
			auto myTransform=vtkSmartPointer<vtkTransform>::New();	//libvtkCommonTransforms
			myTransform->SetMatrix(myMatrix);
			//[3] フィルターを定義
			auto myFilter=vtkSmartPointer<vtkTransformFilter>::New();//libvtkFiltersGeneric,libvtkFiltersGeneral
			myFilter->SetInputData(vtkData);	//require libvtkCommonExecutionModel
			myFilter->SetTransform(myTransform);
			myFilter->Update();
//			vtkData->ShallowCopy(myFilter->GetOutput());
			vtkData->DeepCopy(myFilter->GetOutput());	//require libvtkCommonSystem
		}
		//データを書き出し
		std::stringstream    fileNumber;
		fileNumber <<  std::setfill('0')<< std::setw(digit_VTK) << nt << ".vtm";
		boost::filesystem::path BODYfile_tmp(BODYfile);
		BODYfile_tmp+=fileNumber.str();
		auto BODYwriter=vtkSmartPointer<vtkXMLMultiBlockDataWriter>::New();
		BODYwriter->SetFileName(BODYfile_tmp.c_str());		//掃除屋にファイル名を設定
		BODYwriter->SetInputData(body);					//掃除屋にボディーを引き渡す
		BODYwriter->Write();							//掃除を実行
		
	}
	return 0;
}
