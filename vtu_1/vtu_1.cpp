//
//  vtu_1.cpp
//  vtu_1
//
//  Created by Hiroshi Sugimoto on 2023/01/17.
//  Copyright © 2023 Hiroshi Sugimoto. All rights reserved.
//
//	Load VTK 2D Unstructured Grid File
//		Copy 2dmesh_test.vtu into $HOME/CGAL_test/ before execution

#include <iostream>
#include <boost/filesystem.hpp>
//Load VTU file
#include <vtkSmartPointer.h>
#include <vtkNew.h>
#include <vtkUnstructuredGrid.h>
#include <vtkXMLUnstructuredGridReader.h>
//Convert to PolyData
#include <vtkGeometryFilter.h>
#include <vtkPolyData.h>
#include <vtkXMLPolyDataWriter.h>
//Add function
#include <vtkDoubleArray.h>
#include <vtkCellData.h>
#include <vtkCell.h>
//Extract Boundary
#include <vtkFeatureEdges.h>	//in vtkFiltersCore, vtkCommonExecutionModel

int main(int argc, const char * argv[]) {
	//Load VTU file
	boost::filesystem::path cpwd=getenv("HOME");
	cpwd/="CGAL_test";
	boost::filesystem::path filename = cpwd/"2dmesh_test.vtu";
	vtkNew<vtkXMLUnstructuredGridReader> reader;
	reader->SetFileName(filename.c_str());
	//Convert to vtkPolyData
	//	https://kitware.github.io/vtk-examples/site/Cxx/PolyData/GeometryFilter/
	auto geometryFilter = vtkSmartPointer<vtkGeometryFilter>::New();
	geometryFilter->SetInputConnection(reader->GetOutputPort());
	//Get Boundary of the data
	//	https://kitware.github.io/vtk-examples/site/Cxx/Meshes/BoundaryEdges/
	vtkNew<vtkFeatureEdges> featureEdges;
	featureEdges->SetInputConnection(geometryFilter->GetOutputPort());
	featureEdges->BoundaryEdgesOn();
	featureEdges->FeatureEdgesOff();
	featureEdges->ManifoldEdgesOff();
	featureEdges->NonManifoldEdgesOff();
	featureEdges->ColoringOn();
	featureEdges->Update();
	vtkPolyData& polydata = *geometryFilter->GetOutput();
	vtkPolyData& polydata_B = *featureEdges->GetOutput();
	std::cout << "Body #Points= " << polydata.GetNumberOfPoints() << " points." << std::endl;
	std::cout << "Body #Cells=  " << polydata.GetNumberOfCells() << " cells." << std::endl;
	std::cout << "Boundary #Points= " << polydata_B.GetNumberOfPoints() << " points." << std::endl;
	std::cout << "Boundary #Cells=  " << polydata_B.GetNumberOfCells() << " cells." << std::endl;
	//Add function to polydata
	vtkDoubleArray& pressure = *vtkDoubleArray::New();		//Create array
	pressure.SetNumberOfComponents(0);						//It is scalar
	pressure.SetName("p");									//Its name
	pressure.SetNumberOfTuples(polydata.GetNumberOfCells());//Its number
	polydata.GetCellData()->AddArray(&pressure);			//Add array
	//Add sample values
	for (int cid = 0; cid < polydata.GetNumberOfCells(); cid++)
	{
		double p=cid;
		pressure.SetValue(cid,p);
	}
	//Save VTP file
	{
		vtkNew<vtkXMLPolyDataWriter> writer;
		boost::filesystem::path p_name = cpwd/"2dmesh_p.vtp";
		writer->SetFileName(p_name.c_str());
		writer->SetInputData(&polydata);
		writer->Write();
	}
	{
		vtkNew<vtkXMLPolyDataWriter> writer;
		boost::filesystem::path p_name = cpwd/"2dmesh_b.vtp";
		writer->SetFileName(p_name.c_str());
		writer->SetInputData(&polydata_B);
		writer->Write();
	}
	return 0;
}
