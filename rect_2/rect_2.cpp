//
//  rect_2: VTKを使って，複数の直方体領域のデータを作成する例
//

#include <boost/filesystem.hpp>
#include <iostream>
#include <vector>
#include <cmath>
#include <sstream>
#include <vtkVersion.h>
#include <vtkSmartPointer.h>
#include <vtkProperty.h>
#include <vtkImageData.h>						//等分割直方格子の領域を使います
#include <vtkMultiBlockDataSet.h>           	//１ケースに多数の領域が含まれる場合，これが必要
#include <vtkXMLMultiBlockDataWriter.h>			//多数の領域を一発で保存する
#include <vtkDoubleArray.h>						//セルや格子点に取り付ける物理量を定義します. libvtkCommonCore 必要
#include <vtkPointData.h>						//格子点のデータ
#include <vtkCellData.h>						//セルのデータ
#include <vtkCell.h>							//セル
#include <vtkInformation.h>						//MetaDataへのアクセス

int main(int argc, const char * argv[]) {
    //時間ステップの作成
    const unsigned int ntMax=10;
    const boost::filesystem::path bodyFolder("rect_2.Body");
    const boost::filesystem::path wallFolder("rect_2.Wall");//同じフォルダーでも良い気がするけど
    boost::filesystem::path bodyStemName("body");//ファイル名はbodyFolder/bodyStemNameN桁の数字extName
    boost::filesystem::path wallStemName("wall");//ファイル名はwallFolder/wallStemNameN桁の数字extName
    const std::string extName=".vtm";        //複数領域では，拡張子は .vtm です
    const int fileN=4;                           //まあテストなので, 4桁あれば十分
    //領域の作成
    const unsigned int domMax=2;                //2領域作成しまーす
    //フォルダーの作成(既存のフォルダーは黙って削除)
    try {
        if (boost::filesystem::exists(bodyFolder)) boost::filesystem::remove_all(bodyFolder);
        if (boost::filesystem::exists(wallFolder)) boost::filesystem::remove_all(wallFolder);
        boost::filesystem::create_directory(bodyFolder);
        boost::filesystem::create_directory(wallFolder);
    } catch (std::exception& e) {
        std::cerr << bodyFolder << " " << wallFolder << " フォルダが作成できんとよ:" << e.what() << std::endl;
        return 1;
    }
    bodyStemName=bodyFolder/bodyStemName;
    wallStemName=wallFolder/wallStemName;
    //時間ステップだけ作業
    for(unsigned int nt=1;nt<=ntMax;nt++){
        //この時刻のデータを保存するMultiBlockDataSetを定義
        auto body=vtkSmartPointer<vtkMultiBlockDataSet>::New();
        auto wall=vtkSmartPointer<vtkMultiBlockDataSet>::New();
        //領域達を定義
        for(int idom=0;idom<domMax;idom++){
            using ImageDataPtr = vtkSmartPointer<vtkImageData>; //行が長くなるので略称alias
            double imageSpacing[3]={0.05,0.05,0.05};//X-Y-Z方向の格子の間隔
            int bodyExtent[6]={0,0,0,0,0,0};         //領域の格子点数．あとで設定します
            double bodyOrigin[3]={0.,0.,0.};        //領域の原点．あとで設定します
            //body形状を定義
            auto ibody=ImageDataPtr::New();//上のaliasを用いてImageDataを作成
            ibody->SetSpacing(imageSpacing);
            switch (idom) {
                case 0:    //領域0のパラメータ
                    bodyExtent[1]=20;        bodyExtent[3]=10;        bodyExtent[5]=10;
                    bodyOrigin[0]=0;         bodyOrigin[1]=0;         bodyOrigin[2]=0;
                    break;
                    ;;
                case 1:    //領域1のパラメータ
                    bodyExtent[1]=10;        bodyExtent[3]=40;        bodyExtent[5]=40;
                    bodyOrigin[0]=1;         bodyOrigin[1]=-0.5;     bodyOrigin[2]=-0.5;
                    break;
                    ;;
            }
            ibody->SetExtent(bodyExtent);
            ibody->SetOrigin(bodyOrigin);
            body->SetBlock(idom,ibody);
            body->GetMetaData(idom)->Set(vtkImageData::FIELD_NAME(),"body");
            //bodyに物理量を定義:ここでは，セルに物理量を準備してみましょう．格子点の場合 Cell をPointに書き換えます
            auto pressure = vtkDoubleArray::New();        //圧力の配列を作成
            auto velocity = vtkDoubleArray::New();        //流速の配列を作成
            pressure->SetNumberOfComponents(0);                        //圧力はスケーラなので成分は無いっす
            velocity->SetNumberOfComponents(3);                        //数学者以外は流速は3成分すね
            pressure->SetName("p");                    //名前を付けておきませう
            velocity->SetName("v");
            velocity->SetComponentName(0,"v_1");
            velocity->SetComponentName(1,"v_2");
            velocity->SetComponentName(2,"v_3");
            pressure->SetNumberOfTuples(ibody->GetNumberOfCells());    //圧力の要素数は, セルの総数
            velocity->SetNumberOfTuples(ibody->GetNumberOfCells());    //流速の要素数は, セルの総数
            ibody->GetCellData()->AddArray(pressure);    //領域のセルデータに，さっき作った圧力を追加
            ibody->GetCellData()->AddArray(velocity);    //さっき作った流速を追加
            //まあ，テストのために値を定義しておきましょう
            double* weight = new double [ibody->GetMaxCellSize()];
            for(vtkIdType cellId=0; cellId < ibody->GetNumberOfCells(); cellId++) {
                //テキトーな値を計算するために, セルの中心座標を取得します.
                //格子点の場合には image->GetPoint(pointId,Xcoord)で取得できますが, セルは面倒
                double Xcoord[3];
                {
                    auto cell=ibody->GetCell(cellId);
                    double Pcoord[3];    //セルは体積要素ですので，内部にいちいち曲線座標が存在します
                    //曲線座標表示で, セルの中心を求めます. 複合セルの場合のsubIdも取得(普通subId=0)
                    int subId= cell->GetParametricCenter(Pcoord);
                    cell->EvaluateLocation(subId,Pcoord,Xcoord,weight); //セル中心のXcoordが取得できました♪
                }
                //値を計算（サンプルなので，テキトーですわ）
                double T=pow(Xcoord[0],2)+pow(Xcoord[1],2)+pow(Xcoord[2],2);
                double p=exp(-0.1*T);
                double v1=0.1;
                double v2=p*Xcoord[1];
                double v3=-0.1;
                //値を設定
                pressure->SetValue(cellId,p);
                velocity->SetComponent(cellId,0,v1);
                velocity->SetComponent(cellId,1,v2);
                velocity->SetComponent(cellId,2,v3);
            }
            //境界形状を定義
            for(int iwall=0;iwall<6;iwall++) {
                //wall形状を定義
                auto image=ImageDataPtr::New();//上のaliasを用いてImageDataを作成
                image->SetSpacing(imageSpacing);          //X-Y-Z方向の格子の間隔
                int wallExtent[6];
                double wallOrigin[6];
                for(int iii=0;iii<6;iii++) {
                    wallExtent[iii]=bodyExtent[iii];
                    wallOrigin[iii]=bodyOrigin[iii];
                }
                std::string name;
                switch (iwall) {
                    case 0: wallExtent[1]=wallExtent[0]; name="left";break;; //左面(X幅がない)
                    case 1: wallExtent[0]=wallExtent[1]; name="right";break;;//右面(X幅がない)
                    case 2: wallExtent[3]=wallExtent[2]; name="floor";break;;//下面(Y幅がない)
                    case 3: wallExtent[2]=wallExtent[3]; name="ceiling";break;;//上面(Y幅がない)
                    case 4: wallExtent[5]=wallExtent[4]; name="away";break;;//後面(Z幅がない)
                    case 5: wallExtent[4]=wallExtent[5]; name="home";break;;//前面(Z幅がない)
                }
                int iBlock=6*idom+iwall;
                image->SetExtent(wallExtent);
                image->SetOrigin(wallOrigin);
                wall->SetBlock(iBlock,image);
                wall->GetMetaData(idom)->Set(vtkImageData::FIELD_NAME(),name.c_str());
                //wallに物理量を定義:ここでは，セルに物理量を準備してみましょう．格子点の場合 Cell をPointに書き換えます
                auto massFlux = vtkDoubleArray::New();      //質量流量の配列を作成
                auto momFlux = vtkDoubleArray::New();		//運動量流量の配列を作成
                massFlux->SetNumberOfComponents(0);         //質量流量はスケーラなので成分は無いっす
                momFlux->SetNumberOfComponents(3);         	//運動量流量は3成分すね
                massFlux->SetName("M");                    	//名前を付けておきませう
                momFlux->SetName("P");
                momFlux->SetComponentName(0,"P_1");
                momFlux->SetComponentName(1,"P_2");
                momFlux->SetComponentName(2,"P_3");
                massFlux->SetNumberOfTuples(image->GetNumberOfCells());    //質量流量の要素数は, セルの総数
                momFlux->SetNumberOfTuples(image->GetNumberOfCells());    //運動量流量の要素数は, セルの総数
                image->GetCellData()->AddArray(massFlux);    //領域のセルデータに，さっき作った圧力を追加
                image->GetCellData()->AddArray(momFlux);    //さっき作った流速を追加
                //まあ，テストのために値を定義しておきましょう
                double* weight = new double [image->GetMaxCellSize()];
                for(vtkIdType cellId=0; cellId < image->GetNumberOfCells(); cellId++) {
                    //テキトーな値を計算するために, セルの中心座標を取得します.
                    //格子点の場合には image->GetPoint(pointId,Xcoord)で取得できますが, セルは面倒
                    double Xcoord[3];
                    {
                        auto cell=image->GetCell(cellId);
                        double Pcoord[3];    //セルは体積要素ですので，内部にいちいち曲線座標が存在します
                        //曲線座標表示で, セルの中心を求めます. 複合セルの場合のsubIdも取得(普通subId=0)
                        int subId= cell->GetParametricCenter(Pcoord);
                        cell->EvaluateLocation(subId,Pcoord,Xcoord,weight); //　ようやく, セル中心のXcoordが取得できました♪
                    }
                    //値を計算（サンプルなので，テキトーですわ）
                    double T=pow(Xcoord[0],2)+pow(Xcoord[1],2)+pow(Xcoord[2],2);
                    double p=sin(-0.1*T);
                    double v1=0.1*p*cos(Xcoord[2]);
                    double v2=p*Xcoord[1];
                    double v3=0.1*p*sin(Xcoord[2]);
                    //値を設定
                    massFlux->SetValue(cellId,p);
                    momFlux->SetComponent(cellId,0,v1);
                    momFlux->SetComponent(cellId,1,v2);
                    momFlux->SetComponent(cellId,2,v3);
                }

            }
            std::cout << "dom[" << idom << "]";
        }
        //      保存を実行
        std::stringstream    fileNumber;
        fileNumber <<  std::setfill('0') <<  std::setw(fileN) << nt << extName;
        {
            boost::filesystem::path fileName(bodyStemName);
            fileName+=fileNumber.str();
            auto writer=vtkSmartPointer<vtkXMLMultiBlockDataWriter>::New();
            writer->SetFileName(fileName.c_str());        	//掃除屋にファイル名を設定
            writer->SetInputData(body);                    	//掃除屋にボディーを引き渡す
            writer->Write();                                   	//掃除を実行
        }
        {
            boost::filesystem::path fileName(wallStemName);
            fileName+=fileNumber.str();
            auto writer=vtkSmartPointer<vtkXMLMultiBlockDataWriter>::New();
            writer->SetFileName(fileName.c_str());           //掃除屋にファイル名を設定
            writer->SetInputData(wall);                        //掃除屋にボディーを引き渡す
            writer->Write();                                      //掃除を実行
        }
        std::cout << " nt=" << nt << " done." << std::endl;
    }
    return 0;
}

